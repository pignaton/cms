<?php

namespace App\Http\Controllers\Admin;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Publication;
use App\Categorie;

class CategorieController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('can:edit-users');
    }

    public function index()
    {
        $categories = [];

        $categories = Categorie::paginate(4);

        return view('admin.categories.index', ['categories' => $categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $categories = [];

        $data = $request->only([
            'title',
            'summary'
        ]);

        $validator = $this->validator($data);

        if ($validator->fails()) {
            return redirect()->route('categories.index')
                ->withErrors($validator)
                ->withInput();
        }

        $categories = new Categorie;

        $categories->title = $data['title'];
        $categories->summary = $data['summary'];
        $categories->save();

        return redirect()->route('categories.index');


        // $categories = Categorie::paginate(4);

        //return view('admin.categories.index', ['categories' => $categories]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Categorie::find($id);

        if ($categories) {
            return view('admin.categories.edit', ['categories' => $categories]);
        }

        return redirect()->route('categories.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $page = Page::find($id);

        if ($page) {

            $datas = $request->only([
                'title',
                'body'
            ]);

            if ($page['title'] !== $datas['title']) {

                $datas['slug'] = Str::slug($datas['title'], '-');

                $validator = Validator::make($datas, [
                    'title' =>  'required|string|max:100',
                    'body' => 'string',
                    'slug' => 'required|string|max:100|unique:pages'
                ]);
            } else {
                $validator = Validator::make($datas, [
                    'title' =>  'required|string|max:100',
                    'body' => 'string'
                ]);
            }

            if ($validator->fails()) {
                return redirect()->route('pages.edit', ['page' => $id])
                    ->withErrors($validator)
                    ->withInput();
            }

            $page->title = $datas['title'];
            $page->body = $datas['body'];

            if (!empty($datas['slug'])) {
                $page->slug = $datas['slug'];
            }

            $page->save();
        }
        return redirect()->route('pages.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $categorie = Categorie::find($id);
        $categorie->delete();

        return redirect()->route('categorie.index');
    }

    public function validator(array $datas)
    {
        return Validator::make($datas, [
            'title' =>  'required|string|min:3|max:100',
            'summary' => 'required|string|min:3',
        ]);
    }
}
