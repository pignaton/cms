<?php

namespace App\Http\Controllers\Admin;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Publication;
use App\Categorie;

class PublicationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $settings = [];

        $publications = Publication::paginate(10);
        $categories = Categorie::get();

        //['settings' => $settings]

        return view('admin.publications.index', ['publications' => $publications, 'categories' => $categories]);
    }

    public function save(Request $request)
    {
    }
}
