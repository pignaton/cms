<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AllTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->tinyInteger('admin')->default(0);
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes('deleted_at');
        });

        Schema::create('visitors', function (Blueprint $table) {
            $table->id();
            $table->string('ip', 100);
            $table->dateTime('date_access');
            $table->string('page', 100);
        });

        Schema::create('settings', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->longText('content', 100);
        });

        Schema::create('pages', function (Blueprint $table) {
            $table->id();
            $table->string('title', 100)->unique();
            $table->string('slug', 100);
            $table->longText('body');
            $table->timestamps();
            $table->softDeletes('deleted_at');
        });

        Schema::create('categories', function (Blueprint $table) {
            $table->id();
            $table->string('title', 100);
            $table->text('summary');
            $table->timestamps();
            $table->softDeletes('deleted_at');
        });

        Schema::create('publications', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_categories');
            $table->string('author', 100);
            $table->string('title', 100);
            $table->text('summary');
            $table->longText('content');
            $table->string('image')->default('default.jpg');
            $table->timestamps();
            $table->softDeletes('deleted_at');

            $table->foreign('id_categories')->references('id')->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
