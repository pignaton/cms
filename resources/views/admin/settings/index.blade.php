@extends('adminlte::page')

@section('title', 'Configurações do site');


@section('content')

    @if ($errors->any())
        <div class="callout callout-danger alert-danger">
            <h4><i class="icon fa fa-ban"></i> Erro!</h4>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="{{ route('settings.save') }}" method="POST" class="form-horizontal">
        @method('PUT')
        @csrf
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <!-- /.card-header -->
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Configurações</h3>
                            </div>
                            <div class="card-body">

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Titulo do Site</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="title" id="title"
                                            value="{{ $settings['title'] }}" aria-describedby="helpId1" placeholder="">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Sub-Titulo do Site</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="subtitle" id="subtitle" class="form-control"
                                            value="{{ $settings['subtitle'] }}" placeholder="" aria-describedby="helpId2">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">E-mail para contato</label>
                                    <div class="col-sm-10">
                                        <input type="email" name="email" id="email" class="form-control"
                                            value="{{ $settings['email'] }}" placeholder="" aria-describedby="helpId3">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Rede Socias</label>
                                    <div class="col-sm-10">
                                        <input type="url" class="form-control" name="title" id="title"
                                            value="{{ $settings['title'] }}" aria-describedby="helpId1" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Cor do fundo</label>
                                    <div class="col-sm-10">
                                        <input type="color" name="bgcolor" id="bgcolor" class="form-control"
                                            value="{{ $settings['bgcolor'] }}" placeholder="" aria-describedby="helpId5"
                                            style="width: 70px;">
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Cor do texto</label>
                                    <div class="col-sm-10">
                                        <input type="color" name="textcolor" id="textcolor" class="form-control"
                                            value="{{ $settings['textcolor'] }}" placeholder="" aria-describedby="helpId6"
                                            style="width: 70px;">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                       <!-- /.and card-header -->
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Rede Sociais</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Facebook</label>
                                    <div class="col-sm-10">
                                        <input type="url" class="form-control" name="title" id="title"
                                            value="" aria-describedby="helpId1" placeholder="">
                                    </div>
                                </div> 

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Twitter</label>
                                    <div class="col-sm-10">
                                        <input type="url" class="form-control" name="title" id="title"
                                            value="" aria-describedby="helpId1" placeholder="">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Instagram</label>
                                    <div class="col-sm-10">
                                        <input type="url" class="form-control" name="title" id="title"
                                            value="" aria-describedby="helpId1" placeholder="">
                                    </div>
                                </div>

                                <div class="form-group row ">
                                    <label class="col-form-label"></label>
                                    <div class="col-sm-10">
                                        <input type="submit" class="btn btn-success" value="Salvar">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
            </div>
    </form>
    </section>
@endsection
