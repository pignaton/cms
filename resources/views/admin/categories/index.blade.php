@extends('adminlte::page')

@section('title', 'Categories')
@section('content_header')
    <h1>
        Categorias
    </h1>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Cadastrar Categoria</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form method="POST" action="{{ route('categories.store') }}">
                        @csrf
                        <div class="card-body">
                            <div class="form-group">
                                <label for="title">Título</label>
                                <input type="text" class="form-control" id="author" name="title" placeholder="Título">
                            </div>
                            <div class="form-group">
                                <label for="summary">Resumo</label>
                                <input type="text" class="form-control" id="title" name="summary" placeholder="Resumo">
                            </div>
                        </div>
                        <!-- /.card-body -->
                        
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Cadastrar</button>
                        </div>
                    </form>
                </div>
            </div>

            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Lista de Categorias</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Título</th>
                                    <th style="width: 40px">Alterar</th>
                                    <th style="width: 40px">Excluir</th>

                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($categories as $categorie)
                                    <tr>
                                        <td>{{ $categorie->id }}</td>
                                        <td>{{ $categorie->title }}</td>
                                        <td> <a href="{{ route('categories.edit', ['categorie' => $categorie->id]) }}"
                                                class="btn btn-sm btn-warning">Editar</a></td>
                                        <td>
                                            <form class="d-inline"
                                                action="{{ route('categories.destroy', ['categorie' => $categorie->id]) }}" method="post"
                                                onsubmit="return confirm('Tem certeza que deseja excluir?')">
                                                @method('DELETE')
                                                @csrf
                                                <button class="btn btn-sm btn-danger">Excluir</button>
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                            </tbody>
                            
                        </table>
                        {{ $categories->links() }}
                    </div>
                </div>
            </div>
        @endsection
