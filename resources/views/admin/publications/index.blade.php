@extends('adminlte::page')

@section('title', 'Usuários')
@section('content_header')
    <h1>
        Minhas Publicações
    </h1>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Adicionar Publicação</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form>
                        <div class="card-body">
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="publish">
                                <label class="form-check-label" for="publish">Publicar de primeira?</label>
                            </div>
                            <div class="form-group">
                                <label for="author">Autor</label>
                                <input type="text" class="form-control" id="author" name="author" placeholder="Autor">
                            </div>
                            <div class="form-group">
                                <label for="titulo">Título</label>
                                <input type="text" class="form-control" id="title" name="title" placeholder="Título">
                            </div>
                            <div class="form-group">
                                <label for="summary">Resumo</label>
                                <input type="text" class="form-control" id="summary" name="summary" placeholder="Resumo">
                            </div>
                            <div class="form-group">
                                <label for="summary">Categoria</label>
                                <select class="form-control" name="categories">
                                    <?php foreach ($categories as $categorie) { ?>
                                    <option value="<?= $categorie['id'] ?>"><?= $categorie['title'] ?></option>
                                        <?php } ?>
                                    </select>
                                  </div>
                                    <div class="form-group">
                                        <label for="content">Conteúdo</label>
                                        <textarea name="content" row=10></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="image">Image</label>
                                        <div class="input-group">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="image">
                                                <label class="custom-file-label" for="image">Choose file</label>
                                            </div>
                                            <div class="input-group-append">
                                                <span class="input-group-text">Upload</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.card-body -->

                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">Cadastrar</button>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Bordered Table</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th style="width: 10px">#</th>
                                            <th>Imagem</th>
                                            <th>Noticia</th>
                                            <th style="width: 40px">Data</th>
                                            <th style="width: 40px">Alterar</th>
                                            <th style="width: 40px">Excluir</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer clearfix">
                                <ul class="pagination pagination-sm m-0 float-right">
                                    <li class="page-item"><a class="page-link" href="#">«</a></li>
                                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                                    <li class="page-item"><a class="page-link" href="#">»</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <script src="/assets/js/tinymce/tinymce.min.js"></script>
                <!--<script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin')"></script>-->
                <script>
                    tinymce.init({
                        language: 'pt_BR',
                        selector: 'textarea',
                        theme: 'modern',
                        plugins: 'print preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount   imagetools contextmenu colorpicker textpattern code',
                        toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat code',
                        image_advtab: true,
                        templates: [{
                                title: 'Test template 1',
                                content: 'Test 1'
                            },
                            {
                                title: 'Test template 2',
                                content: 'Test 2'
                            }
                        ],
                        content_css: [
                            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                            '//www.tinymce.com/css/codepen.min.css'
                        ]
                    });

                </script>
    @endsection
